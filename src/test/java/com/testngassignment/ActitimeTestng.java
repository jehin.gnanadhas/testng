package com.testngassignment;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class ActitimeTestng {
	WebDriver driver;
	 @BeforeClass
	    void setUp() {
	        driver = new ChromeDriver();
	        driver.manage().window().maximize();
	    }

	    /* Quit the driver after execution */
	    @AfterClass
	    void tearDown() {
	        driver.quit();
	    }
  @Test(priority=1)
  public void verifyText() {
	  driver.get("https://demo.actitime.com/login.do");
	  String actualtext = driver.findElement(By.id("headerContainer")).getText();
	  System.out.println(actualtext);
	 String expectedtext="please identify yourself";
	 if(actualtext.equalsIgnoreCase(expectedtext))
	 {
		 System.out.println("pass");
	 }
	 else
	 {
		 System.out.println("fail");
	 }
	  
  }
  @Test(priority=2)
  public  void verifylogo() {
	//driver.findElement("//div[@class='atLogoImg']").
	  boolean displayed = driver.findElement(By.xpath("//div[@class='atLogoImg']")).isDisplayed();
	  System.out.println(displayed);
	  if(displayed==true)
	  {
		  System.out.println("logo is present");
	  }
	  else
	  {
		  System.out.println("logo is not preent");
	  }
}
 
	
  
}

